<?php

namespace Dse\ElementsBundle\ElementTeasertags\ContaoManager;

use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Dse\ElementsBundle\ElementTeasertags;
use Contao\CoreBundle\ContaoCoreBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(ElementTeasertags\DseElementTeasertags::class)
                ->setLoadAfter([ContaoCoreBundle::class])
        ];
    }
}
