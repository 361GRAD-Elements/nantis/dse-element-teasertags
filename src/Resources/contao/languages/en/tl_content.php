<?php

/**
 * 361GRAD Element Teaser Simple
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_teasertags'] = ['Teaser with Tags', 'Teaser Block.'];

$GLOBALS['TL_LANG']['tl_content']['teasersimple_legend']   = 'Teaser settings';

$GLOBALS['TL_LANG']['tl_content']['dse_text']      = ['Text', ''];

$GLOBALS['TL_LANG']['tl_content']['dse_image']     = ['Image', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_imageSize'] = ['Image size', ''];

$GLOBALS['TL_LANG']['tl_content']['teasertags_legend']   = 'Tags settings';
$GLOBALS['TL_LANG']['tl_content']['dse_isTags']      = ['Enable tags', 'Check this if you want to use tags'];
$GLOBALS['TL_LANG']['tl_content']['dse_teaser_tags']      = ['Tags', ''];
$GLOBALS['TL_LANG']['tl_content']['tt_field_1']      = ['Link text', 'The link text will be displayed instead of the target URL.'];
$GLOBALS['TL_LANG']['tl_content']['tt_field_2']      = ['Link', 'Please enter a web address (http://…), an e-mail address (mailto:…) or an insert'];
$GLOBALS['TL_LANG']['tl_content']['tt_field_3']      = ['Link target', 'Open the link in a new browser window.'];
$GLOBALS['TL_LANG']['tl_content']['tt_field_4']      = ['Link Anchor', ''];