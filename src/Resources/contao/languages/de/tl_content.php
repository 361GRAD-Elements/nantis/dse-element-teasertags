<?php

/**
 * 361GRAD Element Teaser Simple
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elemente';
$GLOBALS['TL_LANG']['CTE']['dse_teasertags'] = ['Teaser mit Tags', 'Teaser Block.'];

$GLOBALS['TL_LANG']['tl_content']['teasersimple_legend']   = 'Teaser-Einstellungen';

$GLOBALS['TL_LANG']['tl_content']['dse_text']      = ['Text', ''];

$GLOBALS['TL_LANG']['tl_content']['dse_image']     = ['Bild', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_imageSize'] = ['Bild Größe', ''];

$GLOBALS['TL_LANG']['tl_content']['teasertags_legend']   = 'Tags-Einstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_isTags']      = ['Tags aktivieren', 'Aktivieren Sie diese Option, wenn Sie Tags verwenden möchten.'];
$GLOBALS['TL_LANG']['tl_content']['dse_teaser_tags']      = ['Tags', ''];
$GLOBALS['TL_LANG']['tl_content']['tt_field_1']      = ['Link Text der linken Spalte', 'Der Linktext wird anstelle der Ziel-URL angezeigt.'];
$GLOBALS['TL_LANG']['tl_content']['tt_field_2']      = ['Link der linken Spalte', 'Bitte geben Sie eine Webadresse (http: // ...), eine E-Mail-Adresse (mailto: ...) oder eine Einfügung ein'];
$GLOBALS['TL_LANG']['tl_content']['tt_field_3']      = ['Linkziel', 'Öffnet den Link in einem neuen Browserfenster.'];
$GLOBALS['TL_LANG']['tl_content']['tt_field_4']      = ['Link Anker', ''];