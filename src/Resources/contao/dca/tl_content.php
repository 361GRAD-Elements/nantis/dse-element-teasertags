<?php

/**
 * 361GRAD Element Teaser Simple
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_teasertags'] =
    '{type_legend},type,headline;' .
    '{teasersimple_legend},dse_text,dse_image,dse_imageSize;' .
    '{link_legend},url,target,dse_linkTitle;' .
    '{teasertags_legend},dse_isTags,dse_teaser_tags;' .
    '{margin_legend},dse_marginTop,dse_marginBottom;' .
    '{invisible_legend:hide},invisible,start,stop';

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_text']        = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_text'],
    'inputType' => 'textarea',
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'clr',
        'rte'       => 'tinyMCE',
    ],
    'sql'       => 'text NULL'
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_image'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_image'],
    'inputType' => 'fileTree',
    'eval'      => [
        'fieldType'  => 'radio',
        'filesOnly'  => true,
        'extensions' => Config::get('validImageTypes'),
        'tl_class'   => 'clr'
    ],
    'sql'       => 'binary(16) NULL'
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_imageSize'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['size'],
    'inputType' => 'imageSize',
    'options'   => System::getContainer()->get('contao.image.image_sizes')->getAllOptions(),
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval'      => [
        'rgxp'               => 'digit',
        'includeBlankOption' => true,
        'tl_class'           => 'w50',
    ],
    'sql'       => "varchar(64) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_isTags'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_isTags'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50 m12',
    ],
    'sql'       => "blob NULL"
];


$GLOBALS['TL_DCA']['tl_content']['fields']['dse_teaser_tags'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_teaser_tags'],
    'exclude'   => true,
    'inputType' => 'multiColumnWizard',
    'eval'      => [
        'tl_class'     => 'clr',
        'columnFields' => [
            'tt_field_1' => [
                'label'     => &$GLOBALS['TL_LANG']['tl_content']['tt_field_1'],
                'inputType' => 'text',
                'eval'      => [
                    'style' => 'width: 90%'
                ],
                'sql'       => "varchar(255) NOT NULL default ''"
            ],
            'tt_field_2' => [
                'label'     => &$GLOBALS['TL_LANG']['tl_content']['tt_field_2'],
                'inputType' => 'text',
                'eval'      => [
                    'style' => 'width: 90%'
                ],
                'wizard'    => [
                    [
                        'tl_content',
                        'pagePicker',
                    ]
                ],
                'sql'       => "varchar(255) NOT NULL default ''"
            ],
            'tt_field_3' => [
                'label'     => &$GLOBALS['TL_LANG']['tl_content']['tt_field_3'],
                'inputType' => 'checkbox',
                'eval'      => [
                    'mandatory' => false,
                    'style' => 'width: 90%'
                ],
                'sql'       => "varchar(255) NOT NULL default ''"
            ],
            'tt_field_4' => [
                'label'     => &$GLOBALS['TL_LANG']['tl_content']['tt_field_4'],
                'inputType' => 'text',
                'eval'      => [
                    'style' => 'width: 90%'
                ],
                'sql'       => "varchar(255) NOT NULL default ''"
            ],
        ]
    ],
    'sql'       => "blob NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop']    = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
